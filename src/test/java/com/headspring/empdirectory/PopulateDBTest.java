/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.headspring.empdirectory.controller.EmployeeController;
import com.headspring.empdirectory.model.Employee;
import com.headspring.empdirectory.model.EmployeeNumber;
import com.headspring.empdirectory.service.EmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/empdirectory-context-test.xml"})
public class PopulateDBTest {

	private static final Logger logger = LoggerFactory.getLogger(PopulateDBTest.class);
	private static final int MAX_EMPLOYEE_COUNT = 300;	//30000;
	
	private static final String[] FIRST_NAMES = {"Sherlock", "Emerson", "Paul", "Joshua", "Sandy", "Kyle", "Mary", "Ellen", "Janey", "David", "Claire", "Cathy", "Joy", "Thomas", "Timothy", "Andrei", "Michael", "Seth", "Jacob", "Jose", "Annabelle", "Rose"};
	private static final String[] LAST_NAMES = {"Bird", "McVicar", "Smith", "Taylor", "Yen", "Jones", "Williams", "Holmes", "Lee", "Wang", "Harper", "Schmidt", "Perez", "Ross", "Rice", "Fisher", "Duncan", "Parker", "DeRozan", "Blair", "Kangas", "Chen"};
	private static final String[] TITLES = {"Database Administrator", "Quality Analyst", "Accountant", "Recruitment Specialist", "Director", "Developer", "Programmer", "Project Manager", "Scrum Master", "Software Architect", "Team Lead", "Business Analyst", "Technical Architect", "Application Designer", "Sales Associate", "Account Executive", "Software Engineer"};
	private static final String[] LOCATIONS = {"Houston", "Galleria", "Katy", "Sugar Land", "Woodlands", "Pearland", "Bellaire"};
	private static final String[] AREA_CODES = {"281", "713", "512", "737", "346", "832", "214", "469", "972", "416", "905"};
	private static final String[] PHONE_TYPES = {"Mobile", "Home", "Office"};
	private static final String EMAIL_DOMAIN = "@someemail.com";
	private Random randomizer = new Random();
	
	@Autowired
	private EmployeeService employeeService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	private long employeeCount;

	@Before
	public void setUp() throws Exception {
		Assert.assertNotNull(employeeService);
		
		employeeCount = employeeService.getEmployeeCount();
		logger.info("Before insert: employeeCount = " + employeeCount);
		
		if (employeeCount != MAX_EMPLOYEE_COUNT) {
			employeeService.removeAllEmployees();
			employeeCount = employeeService.getEmployeeCount();
			Assert.assertTrue("Before insert: Employee count should be zero but is " + employeeCount, employeeCount == 0);
		}
	}

	@Test
	public void testCreateEmployees() {

		if (employeeCount != MAX_EMPLOYEE_COUNT) {
			// Create employee test records
			for (int i=0; i<MAX_EMPLOYEE_COUNT; i++) {
				Employee employee = new Employee();
				employee.setFirstName(FIRST_NAMES[randomizer.nextInt(22)] + i);
				employee.setLastName(LAST_NAMES[randomizer.nextInt(22)] + i);
				employee.setTitle(TITLES[randomizer.nextInt(17)]);
				employee.setLocation(LOCATIONS[randomizer.nextInt(7)]);
				employee.setEmail(employee.getFirstName() + employee.getLastName() + EMAIL_DOMAIN);
	 
				// Create phone numbers
				Set<EmployeeNumber> numbers =  new HashSet<EmployeeNumber>();
				int totalPhones = randomizer.nextInt(4);	// each employee has zero, one, two or three phone numbers
				int phoneCounter = 0;
				for (int j=0; j<totalPhones; j++) {
					EmployeeNumber employeeNumber = new EmployeeNumber();
					employeeNumber.setEmployee(employee);
					phoneCounter++;
					employeeNumber.setPhoneId(phoneCounter);
					employeeNumber.setPhoneType(PHONE_TYPES[randomizer.nextInt(3)]);
					String numberFirstPart = getRandomDigit() + getRandomDigit() + getRandomDigit();
					String numberSecondPart = getRandomDigit() + getRandomDigit() + getRandomDigit() + getRandomDigit();
					String phoneNumber = "(" + AREA_CODES[randomizer.nextInt(11)] + ") " + numberFirstPart + "-" + numberSecondPart;
					employeeNumber.setPhoneNumber(phoneNumber);
					numbers.add(employeeNumber);
				}
				employee.setNumbers(numbers);
				logger.info("Employee " + employee.getFirstName() + " " + employee.getLastName() + " has " + totalPhones + " numbers.");
				employeeService.addEmployee(employee);
			}

			employeeCount = employeeService.getEmployeeCount();
			Assert.assertTrue("After insert: Employee count should be greater than zero but is " + employeeCount, employeeCount > 0);
		}

		Assert.assertTrue("Employee count should be " + MAX_EMPLOYEE_COUNT, employeeCount == MAX_EMPLOYEE_COUNT);
	}

	@After
	public void tearDown() throws Exception {
	}

	private String getRandomDigit() {
		return String.valueOf(randomizer.nextInt(10));
	}
}
