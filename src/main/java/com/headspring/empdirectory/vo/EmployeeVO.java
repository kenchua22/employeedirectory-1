/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.vo;

import java.util.Set;

import com.headspring.empdirectory.model.EmployeeNumber;

public class EmployeeVO {

    private int employeeId;
    private String firstName;
    private String lastName;
    private String title;
    private String location;
    private String email;
    private Set<EmployeeNumberVO> numbers;
    
	public int getEmployeeId() {
		return employeeId;
	}
	
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public Set<EmployeeNumberVO> getNumbers() {
		return numbers;
	}

	public void setNumbers(Set<EmployeeNumberVO> numbers) {
		this.numbers = numbers;
	}

}
