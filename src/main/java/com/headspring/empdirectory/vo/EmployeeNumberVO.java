/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.vo;


public class EmployeeNumberVO {
	
	private long id;
	private int phoneId;
	private String phoneType;
	private String phoneNumber;
	private EmployeeVO employee;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getPhoneId() {
		return phoneId;
	}
	
	public void setPhoneId(int phoneId) {
		this.phoneId = phoneId;
	}
	
	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public EmployeeVO getEmployee() {
		return employee;
	}
	
	public void setEmployee(EmployeeVO employee) {
		this.employee = employee;
	}

}
