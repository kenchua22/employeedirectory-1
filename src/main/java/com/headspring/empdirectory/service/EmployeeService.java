/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.service;

import java.util.List;

import com.headspring.empdirectory.model.Employee;

public interface EmployeeService {

    public void addEmployee(Employee employee);
    public void updateEmployee(Employee employee);
    public List<Employee> listEmployees();
    public Employee getEmployeeById(int id);
    public void removeEmployee(int id);
    public long getEmployeeCount();
    public void removeAllEmployees();
}
