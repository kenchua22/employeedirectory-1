/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.headspring.empdirectory.dao.EmployeeDAO;
import com.headspring.empdirectory.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeDAO employeeDAO;

	@Transactional
	public void addEmployee(Employee employee) {
		employeeDAO.addEmployee(employee);
	}

	@Transactional
	public void updateEmployee(Employee employee) {
		employeeDAO.updateEmployee(employee);
	}

	@Transactional
	public List<Employee> listEmployees() {
		List<Employee> employees = employeeDAO.listEmployees();
		return employees;
	}

	@Transactional
	public Employee getEmployeeById(int id) {
		Employee employee = employeeDAO.getEmployeeById(id);
		return employee;
	}

	@Transactional
	public void removeEmployee(int id) {
		employeeDAO.removeEmployee(id);
	}

	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
		this.employeeDAO = employeeDAO;
	}

	public long getEmployeeCount() {
		return employeeDAO.getEmployeeCount();
	}

	@Transactional
	public void removeAllEmployees() {
		employeeDAO.removeAllEmployees();
	}
}
