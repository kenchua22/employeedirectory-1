/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.headspring.empdirectory.model.Employee;

/**
 * Note that this class is not using Hibernate Transaction, as that is being taken cared of by the Spring framework.
 * @author ken
 *
 */
@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);
	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public List<Employee> listEmployees() {
        Session session = this.sessionFactory.getCurrentSession();
        logger.info("Executing query to retrieve employees...");
        List<Employee> employees = session.createQuery("from Employee order by emp_id asc").list();
        logger.info(String.format("%s %d %s", "Successfully retrieved", employees.size(), "employees."));
        return employees;
	}
	
	public void addEmployee(Employee employee) {
        Session session = this.sessionFactory.getCurrentSession();
        logger.info("Adding employee...");
        session.persist(employee);
        session.flush();
        session.clear();
        logger.info(String.format("%s %s", "Successfully added employee", employee));
	}

	public void updateEmployee(Employee employee) {
        Session session = this.sessionFactory.getCurrentSession();
       
        logger.info("Updating employee...");
        session.update(employee);
        session.flush();
        session.clear();
        logger.info(String.format("%s %s", "Successfully updated employee", employee));
	}

	public Employee getEmployeeById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        logger.info(String.format("%s %d", "Retrieving employee", id));
        Employee employee = (Employee) session.load(Employee.class, new Integer(id));
        logger.info(String.format("%s %s", "Successfully retrieved employee", employee));
        return employee;
	}

	public void removeEmployee(int id) {
	    Session session = this.sessionFactory.getCurrentSession();
	    Employee employee = (Employee) session.load(Employee.class, new Integer(id));
        if (null != employee) {
        	logger.info(String.format("%s %d", "Removing employee", id));
            session.delete(employee);
            session.flush();
            session.clear();
        }
        logger.info(String.format("%s %d", "Successfully removed employee", id));
	}

	public long getEmployeeCount() {
		Session session = this.sessionFactory.openSession();
		logger.info("Executing query to count employees...");
		Query query = session.createQuery("select count(*) from Employee");
		Long count = (Long) query.uniqueResult();
		logger.info(String.format("%s %d", "Employee count:", count));
		session.close();
		return count;
	}

	public void removeAllEmployees() {
        Session session = this.sessionFactory.getCurrentSession();
        logger.info("Executing query to remove all employees...");
        int result = session.createQuery("delete from Employee").executeUpdate();
        logger.info(String.format("%s %d", "Delete result:", result));
	}

}
