/**
 * An Employee Directory test app for the Skills Assessment.
 */
package com.headspring.empdirectory.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.headspring.empdirectory.model.Employee;
import com.headspring.empdirectory.model.EmployeeNumber;
import com.headspring.empdirectory.service.EmployeeService;
import com.headspring.empdirectory.vo.EmployeeNumberVO;
import com.headspring.empdirectory.vo.EmployeeVO;

/**
 * This class takes care of client requests by invoking CRUD operations thru the EmployeeService class.
 * @author ken
 *
 */
@RestController
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	private EmployeeService employeeService;

	@Autowired
	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody 
    public List<Employee> listEmployees(Model model) {
		logger.info("Listing employees...");
    	List<Employee> employees = employeeService.listEmployees();
    	logger.info("Returning employees collection in JSON format.");
    	return employees;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    @ResponseBody
    public EmployeeVO editEmployee(@PathVariable("id") int id, Model model) {
    	logger.info(String.format("%s %d", "Editing employee", id));
    	Employee employee = employeeService.getEmployeeById(id);

    	EmployeeVO employeeVO = new EmployeeVO();
    	try {
			employeeVO.setEmployeeId(id);
    		BeanUtils.copyProperties(employeeVO, employee);
		} catch (Exception e) {
			logger.error("Exception occurred copying Employee Entity bean to Value Object.");
		}
    	return employeeVO;
    }
    
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    public void addEmployee(@RequestBody Employee employee, Model model) {
		logger.info("Adding employee " + employee);
		employeeService.addEmployee(employee);
    }

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
    public void saveEmployee(@RequestBody Employee employee, Model model) {
		logger.info("Updating employee " + employee);
		employeeService.updateEmployee(employee);
    }
	
    @RequestMapping("/remove/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public void removeEmployee(@PathVariable("id") int id) {
    	logger.info(String.format("%s %d", "Removing employee", id));
    	employeeService.removeEmployee(id);
    }

    @RequestMapping("/count")
    public String countEmployees(Model model) {
    	logger.info(String.format("%s", "Counting employees..."));
    	long employeeCount = employeeService.getEmployeeCount();
    	model.addAttribute("employeeCount", employeeCount);
    	
        return "test";
    }
}
