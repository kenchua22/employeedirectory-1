'use strict';

var empdirApp = angular.module('empdirApp', ['ngRoute', 'empdirApp.controllers', 'angularUtils.directives.dirPagination', 'empdirApp.formdirectives']).
	config(['$routeProvider', function ($routeProvider) {
		$routeProvider.when('/', {templateUrl: 'employee-list.html', controller: 'EmployeeListController'});
		$routeProvider.when('/add-employee', {templateUrl: 'employee-form.html', controller: 'AddEmployeeFormController as form'});
		$routeProvider.when('/edit-employee', {templateUrl: 'employee-form.html', controller: 'EditEmployeeFormController as form'});
	    $routeProvider.otherwise({redirectTo: '/'});
}]);

empdirApp.value('globalVars', {
    formMode: '',
    empId: 0
});