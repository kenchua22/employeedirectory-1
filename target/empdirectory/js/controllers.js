'use strict';

var app = angular.module('empdirApp.controllers', []);

app.run(function ($rootScope, $templateCache) {
    $rootScope.$on('$viewContentLoaded', function () {
        $templateCache.removeAll();
    });
});

app.controller("EmployeeListController", ['$scope', '$http', '$location', 'globalVars',
  function($scope, $http, $location, globalVars) {
	$scope.currentPage = 1;
	$scope.pageSize = 30;
	$scope.employees = [];
	$scope.delEmpSuccessMsg = false;
    $scope.status;
    
	$scope.deleteEmployee = function (arrayIdx, empId, empFirstName, empLastName) {
		bootbox.confirm("Are you sure you want to delete employee " + empFirstName + " " + empLastName + "?", function(result) {
			if (result) {
				console.log('EmployeeListController removing employee ' + empId + ' at index ' + arrayIdx);
				
				$http({
				    method: 'GET',
				    url: 'app/remove/' + empId,
				
				}).success(function(data, status, headers, config) {
					$scope.status = data;
					console.log('EmployeeListController successfully removed employee. Received status ' + status);

					$scope.employees.splice(arrayIdx, 1);
					console.log('Removed employee index ' + arrayIdx + ' from scope.');
					$scope.employeeCount = $scope.employees.length;
					
				}).error(function(data, status, headers, config) {
					console.log('Error deleting employee. Received status ' + status);
				});				  
			  }
		}); 
    };

	$scope.addEmployee = function () {
		globalVars.formMode = 'add';
		$location.path('/add-employee');
    };
    
	$scope.editEmployee = function (empId) {
		globalVars.formMode = 'edit';
		globalVars.empId = empId;
		$location.path('/edit-employee');
    };
    
    $http({
        method: 'GET',
        url: 'app/list'

    }).success(function(data, status, headers, config) {
		$scope.employees = data;
		$scope.employeeCount = $scope.employees.length;
		console.log('EmployeeListController successfully retrieved ' + $scope.employeeCount + ' employees.');
		
    }).error(function(data, status, headers, config) {
		console.log('Error retrieving employees.');
    });

  }

]);


app.controller("AddEmployeeFormController", ['$scope', '$http', '$location', 'globalVars', '$timeout', 'PhoneType',
  function($scope, $http, $location, globalVars, $timeout, PhoneType) {
	$scope.formMode = globalVars.formMode;
	console.log('AddEmployeeFormController formMode=' + globalVars.formMode);
	$scope.data = {};
	$scope.data.numbers = [];
	$scope.phoneId = 0;
	$scope.success = false;
	$scope.phoneTypes = PhoneType.items;
    var self = this;
    
    $scope.submit = function() {
    	var formData = {
			"email" : $scope.data.email,
			"firstName" : $scope.data.firstName,
			"lastName" : $scope.data.lastName,
			"location" : $scope.data.location,
			"title" : $scope.data.title,
			"numbers" : $scope.data.numbers
		};

		bootbox.confirm("Are you sure you want to add this employee?", function(result) {
			if (result) {
		        console.log('AddEmployeeFormController adding employee ..');
				
				$http.post("app/add", formData).then(function() {
					console.log('Employee added successfully.');
					$scope.success = true;
					$scope.reset();
					$timeout(function() {
					    $(".alert-success").fadeTo(500, 0).slideUp(500, function(){
					        $(this).remove(); 
					    });
					}, 3000);
				}, function(response) {
					console.log('Error adding employee.');
				});			  
			  }
		}); 

    };

	$scope.save = function() {
		$scope.$broadcast('show-errors-check-validity');
		
		if ($scope.empForm.$valid) {
			console.log('Form is valid.');
			$scope.submit();
		} else {
			console.log('Form is not valid.');
		}
	};
	  
	$scope.reset = function() {
		console.log('Resetting form to blank.');
		$scope.$broadcast('show-errors-reset');
		$scope.data = {};
		$scope.data.numbers = [];
		$scope.phoneId = 0;
	};

	$scope.removeNumber= function() {
		var lastItem = $scope.data.numbers.length-1;
		console.log('Removing number ' + lastItem);
		$scope.data.numbers.splice(lastItem);
		$scope.decrementPhoneId();
	};
	  
	$scope.addNumber = function() {
		var newItem = $scope.data.numbers.length + 1;
		$scope.incrementPhoneId();
		$scope.data.numbers.push({"id":0, "phoneId":$scope.phoneId});
	}
	
	$scope.incrementPhoneId = function() {
		$scope.phoneId++;
		console.log('Next phone id:' + $scope.phoneId);
	}

	$scope.decrementPhoneId = function() {
		$scope.phoneId--;
		console.log('Next phone id:' + $scope.phoneId);
	}
	
  }

]);


app.controller("EditEmployeeFormController", ['$scope', '$http', '$location', 'globalVars', '$timeout', 'PhoneType',
  function($scope, $http, $location, globalVars, $timeout, PhoneType) {
	$scope.formMode = globalVars.formMode;
	$scope.data = {};
	$scope.origData = {};
	$scope.phoneId = 0;
	$scope.success = false;
	$scope.phoneTypes = PhoneType.items;
	
    console.log('EditEmployeeFormController editing employee ' + globalVars.empId);
	$http({
	    method: 'GET',
	    url: 'app/edit/' + globalVars.empId,
	
	}).success(function(data, status, headers, config) {
		console.log('EditEmployeeFormController successfully retrieved employee. data = ' + data);
		$scope.data = data; 
		$scope.phoneId = $scope.data.numbers.length;
		console.log("Employee has " + $scope.phoneId + " numbers.");
		// Perform a deep clone to keep the original copy
		$scope.origData = angular.copy($scope.data);
		
	}).error(function(data, status, headers, config) {
		console.log('Error retrieving employee for edit. Received status ' + status);
	});	

    $scope.submit = function() {
    	var formData = {
			"employeeId" : $scope.data.employeeId,
			"email" : $scope.data.email,
			"firstName" : $scope.data.firstName,
			"lastName" : $scope.data.lastName,
			"location" : $scope.data.location,
			"title" : $scope.data.title,
			"numbers" : $scope.data.numbers
		};

		bootbox.confirm("Are you sure you want to modify this employee?", function(result) {
			if (result) {
		        console.log('EditEmployeeFormController updating employee ..');
				
				$http.post("app/edit", formData).then(function() {
					console.log('Employee updated successfully.');
					$scope.success = true;
					// The latest data is now the original
					$scope.origData = angular.copy($scope.data);
					$scope.$broadcast('show-errors-reset');
					$timeout(function() {
					    $(".alert-success").fadeTo(500, 0).slideUp(500, function(){
					        $(this).remove(); 
					    });
					}, 3000);
					
					$timeout(function() {
					    $(".alert-success").fadeTo(500, 0, "swing", function(){
					    	$scope.success = false;
					    });
					}, 3000);
					
				}, function(response) {
					console.log('Error updating employee.');
				});			  
			  }
		}); 

    };

	$scope.save = function() {
		$scope.$broadcast('show-errors-check-validity');
		
		if ($scope.empForm.$valid) {
			console.log('Form is valid.');
			$scope.submit();
		} else {
			console.log('Form is not valid.');
		}
	};
	  
	$scope.reset = function() {
		console.log('Resetting form to original values.');
		$scope.data = angular.copy($scope.origData);
		$scope.$broadcast('show-errors-reset');
		$scope.phoneId = $scope.data.numbers.length;
	};

	$scope.removeNumber= function() {
		var lastItem = $scope.data.numbers.length-1;
		console.log('Removing number ' + lastItem);
		$scope.data.numbers.splice(lastItem);
		$scope.decrementPhoneId();
	};
	  
	$scope.addNumber = function() {
		var newItem = $scope.data.numbers.length + 1;
		$scope.incrementPhoneId();
		$scope.data.numbers.push({"id":0, "phoneId":$scope.phoneId});
	};

	$scope.incrementPhoneId = function() {
		$scope.phoneId++;
		console.log('Next phone id:' + $scope.phoneId);
	}

	$scope.decrementPhoneId = function() {
		$scope.phoneId--;
		console.log('Current phone id:' + $scope.phoneId);
	}
	
  }

]);

app.factory('PhoneType', function() {
	//var items = [{name: 'Home'}, {name: 'Mobile'}, {name: 'Office'}];
	var items = ['Home', 'Mobile', 'Office'];
	return ({items: items});   
});



/*  The dir-paginate directive is compatible with $index, $first, $middle, $last etc. However, $index will only tell you the index on the current page. 
	So if you have 10 items per page, $index will always be between 0 and 9, no matter which page you are on.
	This controller computes for the actual index number.
*/
app.controller("PaginateRowController", ['$scope', 
  function($scope) {
	//$scope.number = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize;
	$scope.number = ($scope.$index) + ($scope.currentPage - 1) * $scope.pageSize;
  }
]);

function OtherController($scope) {
	$scope.pageChangeHandler = function(num) {
		console.log('Switching to page ' + num);
	};
}

app.controller('OtherController', OtherController);
	