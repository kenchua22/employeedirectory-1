<!DOCTYPE html>
<html lang="en" ng-app="empdirApp">

<head>
	<title>Employee Directory App</title>

	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootbox.js"></script>
	<script src="js/angular.js"></script>
	<script src="js/angular-resource.js"></script>
	<script src="js/angular-route.js"></script>
	<script src="js/angular-animate.js"></script>
	<script src="js/app.js"></script>
	<script src="js/controllers.js"></script>
	<script src="js/form-directives.js"></script> -->
	<script src="js/dirPagination.js"></script>

	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/style.css" />
	
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="jumbotron">
					<h2>Employee Directory App</h2>
					<p>AngularJS + Bootstrap + Spring REST + Hibernate + MySQL</p>
					<p><a href="main.html" class="btn btn-primary btn-lg" role="button">Demo</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
