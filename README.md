EmployeeDirectory-1

This Employee Directory app is built with the following technologies:
AngularJS
Bootstrap
Spring REST
Hibernate
MySQL
Tomcat
Maven

Application Features:
Client-side pagination of Employee records.
Search Employees by First Name, Last Name, Title, Location, Email and Phone Numbers.
Add new Employee.
Update existing Employee.
Delete existing Employee.

Environment Setup:
Import project into Eclipse
Execute DDL script 
Run JUnit class PopulateDBTest to insert test records.